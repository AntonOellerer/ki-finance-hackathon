from gensim import corpora, models, similarities
from collections import defaultdict

dictionaryPath = "/tmp/trumpTweets.dict"
corpusPath = "/tmp/trumpTweets.mm"
indexPath = "/tmp/trumpTweets.index"

def train(tweets):
    stoplist = set('for a of the and to in'.split())
    texts = [[word for word in tweet.lower().split() if word not in stoplist] for tweet in tweets]
    frequency = defaultdict(int)
    for text in texts:
        for token in text:
            frequency[token] += 1
    texts = [[token for token in text if frequency[token] > 1] for text in texts]
    dictionary = corpora.Dictionary(texts)
    dictionary.save(dictionaryPath)
    corpus = [dictionary.doc2bow(text) for text in texts]
    corpora.MmCorpus.serialize(corpusPath, corpus)
    lsi = models.LsiModel(corpus, id2word=dictionary, num_topics=2)
    index = similarities.MatrixSimilarity(lsi[corpus])
    index.save(indexPath)

def similarity(tweet):
    dictionary = corpora.Dictionary.load(dictionaryPath)
    index = similarities.MatrixSimilarity.load(indexPath)
    corpus = corpora.MmCorpus(corpusPath)
    vec_bow = dictionary.doc2bow(tweet.lower().split())
    vec_lsi = lsi[vec_bow]
    sims = index[vec_lsi]
    sims = sorted(enumerate(sims), key=lambda item: -item[1])
    return sims[0]
