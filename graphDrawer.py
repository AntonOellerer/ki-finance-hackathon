import networkx as nx
import matplotlib.pyplot as plt

def drawGraph(users):
    graph = nx.DiGraph()
    for user in users
        graph.add_weighted_edges_from(user["followers"])
    nx.draw(graph)
    plt.show()
